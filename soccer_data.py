from file_reader import DataExtracter
from dataAnalyzer import DataAnalyzer


class Smallest_diff_for_against(DataExtracter, DataAnalyzer):
    def __init__(self, path):
        self.path = path

    def get_smallest_diff(self):
        data_extracter = DataExtracter()
        space_splited_list = data_extracter.bring_data(self.path)
        goals_for = 6
        goals_against = 8
        for_team = 1
        difference = DataAnalyzer.difference_between_rows(
            goals_for, goals_against, for_team, space_splited_list)

        difference.sort()

        team = difference[0][1]
        smallest_differance = difference[0]

        print("Team with smallest difference = {}, Smallest difference = {}".format(team, smallest_differance))


if __name__ == "__main__":
    obj1 = Smallest_diff_for_against('football.dat')
    obj1.get_smallest_diff()

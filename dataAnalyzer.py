class DataAnalyzer():
    def difference_between_rows(from_ind, to_ind, whome, space_splited_list):
        difference = []
        for cur_list in space_splited_list:
            if(len(cur_list) == 1):
                continue
            difference.append((abs(float(
                cur_list[from_ind].strip('*')) - float(cur_list[to_ind].strip(
                    '*'))), cur_list[whome]))
        return difference

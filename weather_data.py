from file_reader import DataExtracter
from dataAnalyzer import DataAnalyzer


class smallest_temperature_spread(DataExtracter, DataAnalyzer):
    def __init__(self, path):
        self.path = path

    def get_smallest_temperature_spread(self):
        data_extracter = DataExtracter()
        space_splited_list = data_extracter.bring_data(self.path)
        max_temp = 1
        min_temp = 2
        day = 0
        temperature_spread = DataAnalyzer.difference_between_rows(
            max_temp, min_temp, day, space_splited_list)

        temperature_spread.sort()

        day = temperature_spread[0][1]
        spread = temperature_spread[0][0]

        print("day = {}, smallest_temperature_spread = {}".format(day, spread))


if __name__ == '__main__':
    ob1 = smallest_temperature_spread('weather.dat')
    ob1.get_smallest_temperature_spread()

import math


class Counter:
    value = 0

    def incr(self):
        self.value += 1

    def decr(self):
        self.value -= 1

    def incrby(self, n):
        self.value += n

    def decrby(self, n):
        self.value -= n


class Triengle:
    points = []

    def add_point(self, x, y):
        self.points.append((x, y))

    def perimeter(self):
        perimeter = math.sqrt((self.points[0][0] - self.points[1][0])**2 + (self.points[0][1] - self.points[1][1])**2) + math.sqrt((self.points[1][0] - self.points[2][0])**2 + (
            self.points[1][1] - self.points[2][1])**2) + math.sqrt((self.points[0][0] - self.points[2][0])**2 + (self.points[0][1] - self.points[2][1])**2)
        return perimeter

    def __eq__(self, triengle):
        for p1, p2 in zip(self.points, triengle.points):
            if(p1 != p2):
                return False
        return True


t1 = Triengle()
t1.add_point(0, 0)
t1.add_point(0, 3)
t1.add_point(4, 0)
print(t1.perimeter())
t2 = Triengle()
t2.add_point(1, 2)
t2.add_point(2, 1)
t2.add_point(1, 5)
print(t2.perimeter())
t3 = Triengle()
t3.add_point(1, 2)
t3.add_point(2, 1)
t3.add_point(1, 5)

print(t1 == t3)
# print(t1.is_equal(t3))
# print(t3.is_equal(t1))
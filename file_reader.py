import csv


class DataExtracter():
    def import_data(self, path):
        data_reader = open(path)
        raw_data = csv.DictReader(data_reader)
        return raw_data

    def extract_data(self, raw_data):
        space_splited_list = []
        for row in raw_data:
            space_splited_list.append([i.strip() for i in list(
                row.values())[0].split()])
        return space_splited_list

    def bring_data(self, path):
        raw_data = self.import_data(path)
        return self.extract_data(raw_data)
